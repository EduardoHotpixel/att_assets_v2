#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    ofSetFrameRate(30);
    gui.setup(); // most of the time you don't need a name
    gui.add(tokenInstructionX.setup("tokenInstructionX", 0, 0, ofGetWidth()));
    gui.add(tokenInstructionY.setup("tokenInstructionY", 0, 0, ofGetHeight()));
    gui.add(instrucccionTokenX.setup("instrucccionTokenX", 0, 0, ofGetWidth()));
    gui.add(instrucccionTokenY.setup("instrucccionTokenY", 0, 0, ofGetHeight()));
    gui.add(welcomeX.setup("welcomeX", 0, 0, ofGetWidth()));
    gui.add(welcomeY.setup("welcomeY", 0, 0, ofGetHeight()));
    gui.add(bienvenidoX.setup("bienvenidoX", 0, 0, ofGetWidth()));
    gui.add(bienvenidoY.setup("bienvenidoY", 0, 0, ofGetHeight()));
    gui.add(welcomeNameX.setup("welcomeNameX", 0, 0, ofGetWidth()));
    gui.add(welcomeNameY.setup("welcomeNameY", 0, 0, ofGetHeight()));
    gui.add(bienvenidoNombreX.setup("bienvenidoNombreX", 0, 0, ofGetWidth()));
    gui.add(bienvenidoNombreY.setup("bienvenidoNombreY", 0, 0, ofGetHeight()));
    gui.add(dataX.setup("dataX", 0, 0, ofGetWidth()));
    gui.add(dataY.setup("dataY", 0, 0, ofGetHeight()));
    gui.add(datosX.setup("datosX", 0, 0, ofGetWidth()));
    gui.add(datosY.setup("datosY", 0, 0, ofGetHeight()));
    gui.add(usedDataX.setup("usedDataX", 0, 0, ofGetWidth()));
    gui.add(usedDataY.setup("usedDataY", 0, 0, ofGetHeight()));
    gui.add(restanteX.setup("restanteX", 0, 0, ofGetWidth()));
    gui.add(restanteY.setup("restanteY", 0, 0, ofGetHeight()));
    gui.add(mbX.setup("mbX", 0, 0, ofGetWidth()));
    gui.add(mbY.setup("mbY", 0, 0, ofGetHeight()));
    gui.add(dataSpriteOffsetX.setup("dataSpriteOffsetX", 0, 0, ofGetWidth()));
    gui.add(dataSpriteOffsetY.setup("dataSpriteOffsetY", 0, 0, ofGetHeight()));
    gui.add(outroMsgX.setup("outroMsgX", 0, 0, ofGetWidth()));
    gui.add(outroMsgY.setup("outroMsgY", 0, 0, ofGetHeight()));
    gui.add(mensajeDeSalidaX.setup("mensajeDeSalidaX", 0, 0, ofGetWidth()));
    gui.add(mensajeDeSalidaY.setup("mensajeDeSalidaY", 0, 0, ofGetHeight()));
    gui.add(outro2MsgX.setup("outro2MsgX", 0, 0, ofGetWidth()));
    gui.add(outro2MsgY.setup("outro2MsgY", 0, 0, ofGetHeight()));
    gui.add(mensaje2DeSalidaX.setup("mensaje2DeSalidaX", 0, 0, ofGetWidth()));
    gui.add(mensaje2DeSalidaY.setup("mensaje2DeSalidaY", 0, 0, ofGetHeight()));
    gui.add(logoX.setup("logox", 0, 0, ofGetWidth()));
    gui.add(logoY.setup("logoY", 0, 0, ofGetHeight()));
    gui.add(idleInstruccionX.setup("idleInstruccionX", 0, 0, ofGetWidth()));
    gui.add(idleInstruccionY.setup("idleInstruccionY", 0, 0, ofGetHeight()));
    gui.add(idleInstruccion2X.setup("idleInstruccion2X", 0, 0, ofGetWidth()));
    gui.add(idleInstruccion2Y.setup("idleInstruccion2Y", 0, 0, ofGetHeight()));
    gui.add(idleInstructionX.setup("idleInstructionX", 0, 0, ofGetWidth()));
    gui.add(idleInstructionY.setup("idleInstructionY", 0, 0, ofGetHeight()));
    gui.add(idleInstruction2X.setup("idleInstruction2X", 0, 0, ofGetWidth()));
    gui.add(idleInstruction2Y.setup("idleInstruction2Y", 0, 0, ofGetHeight()));
    gui.add(timerScene.setup("timerScene", 0, 0, 30000));
    gui.loadFromFile("settings.xml");
    
    //SYPHON
    mainOutputSyphonServer.setName("ATTSmartBuilding");
    
    //TEXT BUFFERS INFO
    serialBuffer = ofBufferFromFile("rfid.txt");
    arduinoBuffer = ofBufferFromFile("arduino.txt");
    
    //OSC
    sender.setup(HOST, PORT);
    receiver.setup(RECEIVER_PORT);
    
    //INIT SERIAL
    serial.setup(serialBuffer.getText(), 9600);
    serial.startContinuousRead();
    ofAddListener(serial.NEW_MESSAGE,this,&ofApp::onNewMessage);
    
    //ARDUINO
    ard.connect(arduinoBuffer.getText(),57600);
    ofAddListener(ard.EInitialized, this, &ofApp::setupArduino);
    bSetupArduino	= false;
    
    screen1State = "OFF";
    screen2State = "OFF";
    screen3State = "OFF";
    screen4State = "OFF";
    
    //TOKEN STATE
    welcomeState = 0;
    //TOKEN PARTICLES
    tokenParticles.load("token/particles.mov");
    tokenParticles.setLoopState(OF_LOOP_NORMAL);
    tokenParticles.play();
    
    //TOKEN ARROW
    arrow.load("token/arrow.mov");
    arrow.setLoopState(OF_LOOP_NORMAL);
    arrow.play();
    //TOKEN FONT
    tokenFont.load("fonts/ATTAleckSans_Lt.ttf", 33);
    //TOKEN ALPHA
    tokenAlpha = 255;
    
    //WELCOME LOGO
    welcomeLogo.load("welcome/logo.mov");
    welcomeLogo.setLoopState(OF_LOOP_NONE);
    welcomeFont.load("fonts/ATTAleckSans_Th.ttf", 100);
    welcomeAlpha = 255;
    logoScale=1.0;
    
    //DATA
    dataDir.listDir("datasprite");
    dataDir.sort(); // in linux the file system doesn't return file lists ordered in alphabetical order
    
    //allocate the vector to have as many ofImages as files
    if( dataDir.size() ){
        dataSprite.assign(dataDir.size(), ofImage());
    }
    
    // you can now iterate through the files and load them into the ofImage vector
    for(int i = 0; i < (int)dataDir.size(); i++){
        dataSprite[i].load(dataDir.getPath(i));
    }
    currentData = 0;
    targetData =  0;
    remainingDataAlpha =0;
    timerDataEnd = false;
    startDataTimer = ofGetElapsedTimeMillis();
    endTimerData = timerScene;
    initialData = 3000;
    
    remainingDataFont.load("fonts/ATTAleckSans_Th.ttf", 90);
    mbDataFont.load("fonts/ATTAleckSans_Th.ttf", 50);
    firstNameFont.load("fonts/ATTAleckSans_Th.ttf", 100);
    dataMsgFont.load("fonts/ATTAleckSans_Lt.ttf", 30);
    transactionFont.load("fonts/ATTAleckSans_Lt.ttf", 30);
    
    outroMsg1.load("fonts/ATTAleckSans_Th.ttf", 100);
    outroMsg2.load("fonts/ATTAleckSans_Lt.ttf", 80);
    
    readerStatus=0;
    firstName = "null";
    
    // IS TOKEN PLACED
    isPlaced == false;
    isEnglish = false;
    showInfo=false;
    isUserActive = false;
    ofHideCursor();
    
    //TCP USER SERVER
    TCP.setup(7001);
    TCP.setMessageDelimiter("\n");
    lastSent = 0;
    
    //TCP LANG USER SERVER
    TCPLang.setup(7002);
    TCPLang.setMessageDelimiter("\n");
    lastSentLang = 0;
    
    pause = false;
    
    idleVideo.load("idle.mov");
    idleVideo.setLoopState(OF_LOOP_NORMAL);
    idleVideo.play();
    
    //BOX
    boxPlaced = false;
    
    
    //DEBUG
    //HTTP LANGJSON
    jsonOK = false;
    queryEndTime = QUERY_INTERVAL;
    queryTimerEnd = false;
    queryStartTime = ofGetElapsedTimeMillis();
}


//--------------------------------------------------------------
void ofApp::update(){
    
    //TCP SERVER
    // for each client lets send them a message letting them know what port they are connected on
    // we throttle the message sending frequency to once every 100ms
    uint64_t now = ofGetElapsedTimeMillis();
    if(now - lastSent >= 100){
        for(int i = 0; i < TCP.getLastID(); i++){
            if( !TCP.isClientConnected(i) ) continue;
            
            TCP.send(i, "ACK hello client - "+ofToString(TCP.getClientPort(i)) );
        }
        lastSent = now;
    }
    
    //RFID
    if(requestRead)
    {
        serial.sendRequest();
        requestRead = false;
    }
    
    
    //ARDUINO
    updateArduino();
    while(receiver.hasWaitingMessages()){
        // get the next message
        ofxOscMessage m;
        receiver.getNextMessage(m);
        
        //MENSAJES OSC MOTOR
        if(m.getAddress() == "/motor/state"){
            if(m.getArgAsInt(0)==1){
                ard.sendDigital(3, ARD_HIGH);
                motorState = "ON";
            }
            if(m.getArgAsInt(0)==0){
                ard.sendDigital(3, ARD_LOW);
                motorState = "OFF";
            }
        }
        //MENSAJES PANTALLA 1
        if(m.getAddress() == "/screen1"){
            
            if(m.getArgAsInt(0)==1){
                ard.sendDigital(4, ARD_HIGH);
                screen1State = "ON";
            }
            
            if(m.getArgAsInt(0)==0){
                ard.sendDigital(4, ARD_LOW);
                screen1State = "OFF";
                
                
            }
            
        }
        //MENSAJES PANTALLA 2
        if(m.getAddress() == "/screen2"){
            
            if(m.getArgAsInt(0)==1){
                ard.sendDigital(5, ARD_HIGH);
                screen2State = "ON";
                
            }
            
            if(m.getArgAsInt(0)==0){
                ard.sendDigital(5, ARD_LOW);
                screen2State = "OFF";
                
                
            }
            
        }
        
        //MENSAJES PANTALLA 3
        if(m.getAddress() == "/screen3"){
            
            if(m.getArgAsInt(0)==1){
                ard.sendDigital(6, ARD_HIGH);
                screen3State = "ON";
                
            }
            
            if(m.getArgAsInt(0)==0){
                ard.sendDigital(6, ARD_LOW);
                screen3State = "OFF";
                
                
            }
            
        }
        //MENSAJES PANTALLA 4
        if(m.getAddress() == "/screen4"){
            
            if(m.getArgAsInt(0)==1){
                ard.sendDigital(7, ARD_HIGH);
                screen4State = "ON";
            }
            
            if(m.getArgAsInt(0)==0){
                ard.sendDigital(7, ARD_LOW);
                screen4State = "OFF";
                
            }
            
        }
    }
    
    
    if(!pause){
        tokenParticles.update();
        arrow.update();
        welcomeLogo.update();
        idleVideo.update();
    }
    
    
    float queryTimer = ofGetElapsedTimeMillis() - queryStartTime;
    
    if( queryTimer >= queryEndTime){
        
        queryTimerEnd= true;
        cout<<"query"<<endl;
        
    }
    
    if(queryTimer>=QUERY_INTERVAL){
        
        url = "http://10.0.1.128:8080/api/lang";
        httpJSON.open(url);
        
        if(httpJSON["lang"].asString()=="es"){
            jsonOK=false;
            isEnglish = false;
            cout<<"espa�ol"<<endl;
            
            
        }else{
            isEnglish = true;
            jsonOK = true;
            cout<<"ingles"<<endl;
            
            
        }
        cout<<"resets query"<<endl;
        queryTimerEnd = false;
        queryStartTime = ofGetElapsedTimeMillis();
        
    }
    
    
    
}

void ofApp::TCPServer(){
    
    ofSetHexColor(0xDDDDDD);
    
    // for each connected client lets get the data being sent and lets print it to the screen
    for(unsigned int i = 0; i < (unsigned int)TCP.getLastID(); i++){
        
        if( !TCP.isClientConnected(i) )continue;
        
        // calculate where to draw the text
        int xPos = 15;
        int yPos = 80 + (12 * i * 4);
        
        // get the ip and port of the client
        string port = ofToString( TCP.getClientPort(i) );
        string ip   = TCP.getClientIP(i);
        string info = "client "+ofToString(i)+" -connected from "+ip+" on port: "+port;
        
        
        // if we don't have a string allocated yet
        // lets create one
        if(i >= storeText.size() ){
            storeText.push_back( string() );
        }
        
        // receive all the available messages, separated by \n
        // and keep only the last one
        string str;
        string tmp;
        do{
            str = tmp;
            tmp = TCP.receive(i);
            
            
        }while(tmp!="");
        
        // if there was a message set it to the corresponding client
        if(str.length() > 0){
            storeText[i] = str;
        }
        if(showInfo==true){
            //draw the info text and the received text bellow it
            ofDrawBitmapString(info, xPos, yPos);
            ofDrawBitmapString(storeText[i], 25, yPos + 20);
        }
        json = storeText[i];
        //cout<<storeText[i]<<endl;
    }
    
    std::string jsonElement[13];
    
    vector<string> splitJSON = ofSplitString( json, ",");
    
    for(int i=0; i<splitJSON.size(); i++){
        //printf( "element %i is %s\n", i, splitJSON[i].c_str() );
        jsonElement[i] =splitJSON[i].c_str();
        
    }
    
    firstName = jsonElement[0].erase(0,14);
    firstName.pop_back();
    lastName = jsonElement[1].erase(0,9);
    genre = jsonElement[2].erase(0,8);
    readerStatus = ofToInt(jsonElement[5].erase(0,9));
    remainingData = jsonElement[8];
    remainingData = jsonElement[8].erase(0,12);
    
    
}

void ofApp::TCPServerLang(){
    
    ofSetHexColor(0xDDDDDD);
    
    // for each connected client lets get the data being sent and lets print it to the screen
    for(unsigned int i = 0; i < (unsigned int)TCPLang.getLastID(); i++){
        
        if( !TCPLang.isClientConnected(i) )continue;
        
        // calculate where to draw the text
        int xPos = 15;
        int yPos = 140 + (12 * i * 4);
        
        // get the ip and port of the client
        string port = ofToString( TCPLang.getClientPort(i) );
        string ip   = TCPLang.getClientIP(i);
        string info = "client "+ofToString(i)+" -connected from "+ip+" on port: "+port;
        
        
        // if we don't have a string allocated yet
        // lets create one
        if(i >= storeTextLang.size() ){
            storeTextLang.push_back( string() );
        }
        
        // receive all the available messages, separated by \n
        // and keep only the last one
        string str;
        string tmp;
        do{
            str = tmp;
            tmp = TCPLang.receive(i);
            
            
        }while(tmp!="");
        
        // if there was a message set it to the corresponding client
        if(str.length() > 0){
            storeTextLang[i] = str;
        }
        
        if(showInfo==true){
            //draw the info text and the received text bellow it
            ofDrawBitmapString(info, xPos, yPos);
            ofDrawBitmapString(storeTextLang[i], 25, yPos + 20);
        }
        jsonLang = storeTextLang[i];
        
    }
    
    //jsonLang="{lang : es}";
    
    std::string jsonElement[10];
    
    vector<string> splitJSON = ofSplitString( jsonLang, ":");
    
    for(int i=0; i<splitJSON.size(); i++){
        //     printf( "element %i is %s\n", i, splitJSON[i].c_str() );
        jsonElement[i] =splitJSON[i].c_str();
        
    }
    
    lang = jsonElement[1];
    lang.pop_back();
    //cout<<lang<<endl;
    
    if(lang.find("en") == true){
        
        isEnglish = true;
        
    }
    
    if(lang.find("es") == true){
        
        isEnglish = false;
        
    }
}




//--------------------------------------------------------------
void ofApp::draw(){
    
    ofBackground(0, 0, 0);
    ofSetColor(255, 255, 255);
    
    remainingDataAlpha = ofClamp(remainingDataAlpha, 0, 255);
    
    switch(welcomeState){
            
        case 0:
            //TOKEN PARTICLES
            tokenAlpha = ofClamp(tokenAlpha , 0 , 255);
            ofSetColor(255,255,255,tokenAlpha);
            tokenParticles.draw(0, 0,tokenParticles.getWidth(), tokenParticles.getHeight());
            
            //ARROW
            ofPushMatrix();
            ofTranslate(100,ofGetHeight()-200);
            ofRotate(45);
            arrow.draw(0,0);
            ofPopMatrix();
            
            if(!isEnglish){
                tokenFont.drawString("Coloque su token donde indica la flecha",instrucccionTokenX,instrucccionTokenY);
            }
            
            if(isEnglish == true){
                tokenFont.drawString("Place your token where the arrow points",tokenInstructionX,tokenInstructionY);
            }
            
            if(readerStatus == 1 && isUserActive == false){
                
                if(readerStatus == 1){
                    
                    tokenAlpha-=10;
                    
                    if(tokenAlpha <=0){
                        welcomeState=1;
                        welcomeLogo.setFrame(0);
                        welcomeLogo.play();
                        isUserActive = true;
                        //readerStatus="[1]}}";
                        
                        
                    }
                }
            }
            
            break;
            
        case 1:
            
            welcomeAlpha = ofClamp(welcomeAlpha , 0 , 255);
            ofSetColor(255,255,255,welcomeAlpha);
            
            welcomeLogo.draw(192,ofGetHeight()/4);
            
            ofPushMatrix();
            
            if(!isEnglish){
                if (genre.find("m")==true){
                    welcomeFont.drawString("Bienvenido", bienvenidoX,bienvenidoY);
                }
                if (genre.find("f") == true){
                    welcomeFont.drawString("Bienvenida", bienvenidoX,bienvenidoY);
                }
            }
            
            if(isEnglish){
                welcomeFont.drawString("Welcome",welcomeX,welcomeY);
            }
            
            ofPopMatrix();
            
            if(!pause){
                
                if (welcomeLogo.getCurrentFrame() >= welcomeLogo.getTotalNumFrames()-15  ){
                    welcomeAlpha -= 50;
                    
                    if(welcomeAlpha<=0){
                        welcomeState = 2;
                        remainingDataAlpha =0;
                        startDataTimer = ofGetElapsedTimeMillis();
                        
                    }
                }
            }
            
            break;
            
        case 2:
            
            targetData = ofToInt(remainingData);
            mappedTargetData = ofMap(targetData, 0, initialData, 0, 99);
            currentData = ofClamp(currentData, 0, mappedTargetData);
            mappedCurrentData = ofMap(currentData, 0, 99, 0, initialData);
            currentData = ofClamp(currentData, 0, 99);
            ofSetColor(0,163,224,remainingDataAlpha*2);
            
            dataSprite[currentData].draw(dataSpriteOffsetX,dataSpriteOffsetY);
            remainingDataFont.drawString(ofToString(mappedCurrentData), restanteX,restanteY);
            mbDataFont.drawString("MB", mbX,mbY);
            
            
            if(!isEnglish){
                dataMsgFont.drawString("Para esta experiencia se van a utilizar", datosX,datosY);
                dataMsgFont.drawString("200 MB de su plan", datosX,datosY+50);
                
            }
            
            if(isEnglish==true){
                dataMsgFont.drawString("200 MB from your plan will be used", dataX,dataY);
                dataMsgFont.drawString("for this experience", dataX,dataY+50);
                
            }
            
            
            if(!pause){
                if(outroAlpha<=endTimerData/2){
                    remainingDataAlpha+=3;
                }
                
                if(outroAlpha>=endTimerData/2){
                    remainingDataAlpha-=3;
                }
            }
            ofSetColor(255, 255, 255,remainingDataAlpha);
            transactionFont.drawString("-200", usedDataX,usedDataY);
            ofSetColor(0,163,224,remainingDataAlpha*2);
            
            
            if(!isEnglish){
                if (genre.find("m") == true){
                    welcomeFont.drawString(firstName, bienvenidoNombreX,bienvenidoNombreY);
                }
                if (genre.find("f") == true){
                    welcomeFont.drawString(ofToString(firstName), bienvenidoNombreX,bienvenidoNombreY);
                }
                
                
            }
            
            if(isEnglish){
                welcomeFont.drawString(firstName,bienvenidoNombreX,bienvenidoNombreY);
            }
            
            if (dataDir.size() > 0){
                if(!pause){
                    currentData++;
                }
                //currentData %= dataDir.size();
            }
            
            if(!pause){
                dataTimer = ofGetElapsedTimeMillis() - startDataTimer;
                outroAlpha =dataTimer;
                if(dataTimer>= endTimerData){
                    timerDataEnd = true;
                    dataAlpha = ofClamp(dataAlpha, 0, 255);
                    tokenAlpha = 255;
                    currentData = 0;
                    remainingDataAlpha = 0 ;
                    welcomeAlpha = 255;
                    welcomeState = 3;
                }
                
            }
            break;
            
        case 3:
            //TOKEN PARTICLES
            tokenAlpha = ofClamp(tokenAlpha , 0 , 255);
            ofSetColor(255,255,255,tokenAlpha);
            idleVideo.draw(0,0);
            
            msgAlpha = ofClamp(msgAlpha,0,255);
            
            if(boxPlaced == true){
                msgAlpha+=0.1;
                
                ofSetColor(255, 255, 255,msgAlpha);
                
                if(!isEnglish){
                    // dataMsgFont.drawString("Coloque un activo o",  idleInstruccionX,idleInstruccionY);
                    dataMsgFont.drawString("Retire su token para terminar",  idleInstruccion2X,idleInstruccion2Y);
                    //ARROW
                    ofPushMatrix();
                    ofTranslate(100,ofGetHeight()-200);
                    ofRotate(45);
                    arrow.draw(0,0);
                    ofPopMatrix();
                    
                }
                
                if(isEnglish==true){
                    
                    //dataMsgFont.drawString("Place an asset or",  idleInstruccionX,idleInstruccionY);
                    dataMsgFont.drawString("Remove your token to finish",  idleInstruccion2X,idleInstruccion2Y);
                    //ARROW
                    ofPushMatrix();
                    ofTranslate(100,ofGetHeight()-200);
                    ofRotate(45);
                    arrow.draw(0,0);
                    ofPopMatrix();
                    
                }
                
            }
            
            
            if(readerStatus==0){
                
                tokenAlpha-=10;
                
                if(tokenAlpha <=0){
                    welcomeState=4;
                    startDataTimer = ofGetElapsedTimeMillis();
                    welcomeLogo.setFrame(0);
                    welcomeLogo.play();
                    ofxOscMessage m1;
                    m1.setAddress("/nuup/show/jumpto");
                    m1.addStringArg("Act_1/Scene_SB");
                    sender.sendMessage(m1);
                    msgAlpha = 0;
                    
                    
                    
                }
            }
            break;
            
        case 4:
            
            targetData = ofToInt(remainingData);
            mappedTargetData = ofMap(targetData, 0, initialData, 0, 99);
            currentData = ofClamp(currentData, 0, mappedTargetData);
            mappedCurrentData = ofMap(currentData, 0, 99, 0, initialData);
            welcomeAlpha = ofClamp(welcomeAlpha , 0 , 255);
            
            ofSetColor(255,255,255,welcomeAlpha);
            welcomeLogo.draw(logoX, logoY);
            
            
            if(!isEnglish){
                outroMsg1.drawString("Hasta luego", mensajeDeSalidaX,mensajeDeSalidaY);
                // outroMsg2.drawString("Por favor, retira tu token", outro2MsgX,outro2MsgY);
                
            }
            
            if(isEnglish==true){
                outroMsg1.drawString("Thank you", outroMsgX,outroMsgY);
                // outroMsg2.drawString("Please, take your token", outro2MsgX,outro2MsgY);
                
            }
            
            if(!pause){
                if(outroAlpha<=endTimerData/2){
                    remainingDataAlpha+=3;
                }
                
                if(outroAlpha>=endTimerData/2){
                    remainingDataAlpha-=3;
                }
            }
            
            if(!pause){
                
                if (welcomeLogo.getCurrentFrame() >= welcomeLogo.getTotalNumFrames()-15  ){
                    welcomeAlpha -= 50;
                    
                    if(welcomeAlpha<=0){
                        ard.sendDigital(3, ARD_LOW);
                        remainingDataAlpha =0;
                        timerDataEnd = true;
                        dataAlpha = ofClamp(dataAlpha, 0, 255);
                        tokenAlpha = 255;
                        currentData = 0;
                        remainingDataAlpha = 0 ;
                        welcomeAlpha = 255;
                        isPlaced = false;
                        remainingDataAlpha = 0 ;
                        isUserActive = false;
                        welcomeState = 0;
                        boxPlaced = false;
                    }
                    
                }
                
            }
            break;
            
    }
    //DEBUG
    if(showInfo == true){
        
        ofDrawBitmapStringHighlight("FPS: "+ofToString(ofGetFrameRate()), ofGetWidth()/2-150,13);
        
        if(serial.isInitialized()){
            ofDrawBitmapStringHighlight("RFID CONECTADO", 0,13);
        }else{
            ofDrawBitmapStringHighlight("CONECTANDO RFID", 0,13);
        }
        
        if (!bSetupArduino){
            
            ofDrawBitmapStringHighlight("CONECTANDO CONTROLADOR DEL MOTOR", ofGetWidth()-260,13);
            
        } else{
            ofDrawBitmapStringHighlight("MOTOR CONECTADO", ofGetWidth()-125,13);
            ofDrawBitmapStringHighlight("MOTOR "+motorState, ofGetWidth()-125,33);
            ofDrawBitmapStringHighlight("PANTALLA 1 "+screen1State, ofGetWidth()-125,53);
            ofDrawBitmapStringHighlight("PANTALLA 2 "+screen2State, ofGetWidth()-125,73);
            ofDrawBitmapStringHighlight("PANTALLA 3 "+screen3State, ofGetWidth()-125,93);
            ofDrawBitmapStringHighlight("PANTALLA 4 "+screen4State, ofGetWidth()-125,113);
        }
        
        gui.draw();
    }
    
    //SYPHON OUTPUT
    mainOutputSyphonServer.publishScreen();
    //TCP SERVERS
    TCPServer();
    TCPServerLang();
    if(showInfo){
        ofLine(960,0,960,ofGetWidth());
        ofLine(1920,0,1920,ofGetWidth());
        ofLine(2880,0,2880,ofGetWidth());
        
    }
    
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    
    if(key=='a'){
        ard.sendDigital(3, ARD_HIGH);
    }
    
    if(key=='A'){
        ard.sendDigital(3, ARD_LOW);
    }
    
    
    
    if(key == 's'){
        gui.saveToFile("settings.xml");
    }
    
    if (key == 'l'){
        
        gui.loadFromFile("settings.xml");
        
    }
    
    if(key=='1'){
        
        isActive1 = !isActive1;
        
        if(isActive1){
            
            ard.sendDigital(4, ARD_HIGH);
            screen1State = "ON";
            
        }else{
            ard.sendDigital(4, ARD_LOW);
            screen1State = "OFF";
            
            
        }
    }
    
    
    if(key=='2'){
        
        isActive2 = !isActive2;
        
        if(isActive2){
            
            ard.sendDigital(5, ARD_HIGH);
            screen2State = "ON";
            
        }else{
            
            ard.sendDigital(5, ARD_LOW);
            screen2State = "OFF";
        }
    }
    
    if(key=='3'){
        
        isActive3 = !isActive3;
        
        if(isActive3){
            
            ard.sendDigital(6, ARD_HIGH);
            screen3State = "ON";
            
        }else{
            ard.sendDigital(6, ARD_LOW);
            screen3State = "OFF";
        }
    }
    
    if(key=='4'){
        
        isActive4 = !isActive4;
        
        if(isActive4){
            
            ard.sendDigital(7, ARD_HIGH);
            screen4State = "ON";
            
        }else{
            
            ard.sendDigital(7, ARD_LOW);
            screen4State = "OFF";
            
            
        }
    }
    if(key=='c'){
        ofHideCursor();
    }
    if (key=='C'){
        ofShowCursor();
    }
    if(key == 'g'){
        showInfo = ! showInfo;
    }
    
    if(key == 'p'){
        
        pause = !pause;
        
        tokenParticles.setPaused(pause);
        arrow.setPaused(pause);
        welcomeLogo.setPaused(pause);
        idleVideo.setPaused(pause);
    }
    if(key=='t'){
        isPlaced = true;
        welcomeState=1;
        welcomeLogo.setFrame(0);
        welcomeLogo.play();
        isUserActive = true;
        
        // json ="{'firstName':'Lola','lastName':'otto','genre':'f','employer':'AT&Tasdasd','reader':{'status':1,'index':1},'data':{'visitedExperience':2,'timeDuration':24,'metersTraveled':2816,'initial':initialData,'transaction':1061,'remaining': 1345}}";
    }
    if(key=='T'){
        ofxOscMessage m1;
        m1.setAddress("/nuup/show/jumpto");
        m1.addStringArg("Act_1/Scene_SB");
        sender.sendMessage(m1);
        json ="{'firstName':'Lola','lastName':'otto','genre':'f','employer':'AT&Tasdasd','reader':{'status':0,'index':1},'data':{'visitedExperience':2,'timeDuration':24,'metersTraveled':2816,'initial':initialData,'transaction':1061,'remaining': 1345}}";
    }
    if(key =='E'){
        isEnglish = true;
    }
    if(key =='e'){
        isEnglish = false;
    }
    
    
    
}

void ofApp::setupArduino(const int & version) {
    
    // remove listener because we don't need it anymore
    ofRemoveListener(ard.EInitialized, this, &ofApp::setupArduino);
    
    // it is now safe to send commands to the Arduino
    bSetupArduino = true;
    
    // print firmware name and version to the console
    ofLogNotice() << ard.getFirmwareName();
    ofLogNotice() << "firmata v" << ard.getMajorFirmwareVersion() << "." << ard.getMinorFirmwareVersion();
    
    // Note: pins A0 - A5 can be used as digital input and output.
    // set pin A0 to analog input
    ard.sendAnalogPinReporting(0, ARD_ANALOG);
    ard.sendAnalogPinReporting(1, ARD_ANALOG);
    ard.sendAnalogPinReporting(2, ARD_ANALOG);
    
    // set pin D13 as digital output
    ard.sendDigitalPinMode(13, ARD_OUTPUT);
    
    // Listen for changes on the digital and analog pins
    ofAddListener(ard.EDigitalPinChanged, this, &ofApp::digitalPinChanged);
    ofAddListener(ard.EAnalogPinChanged, this, &ofApp::analogPinChanged);
}

//--------------------------------------------------------------
void ofApp::updateArduino(){
    
    ard.update();
    
    
}

//--------------------------------------------------------------
void ofApp::digitalPinChanged(const int & pinNum) {}
//--------------------------------------------------------------
void ofApp::analogPinChanged(const int & pinNum) {}


void ofApp::onNewMessage(string & message)
{
    if(message.find("7C00565A81F1" )==true || message.find("7C0056935DE4")==true || message.find("7C00564692FE")==true || message.find("7C0056720B53")==true || message.find("7C0056644709")==true ||message.find("7C0056596615")==true){
        ofBackground(ofRandom(255),ofRandom(255),ofRandom(255));
        ofxOscMessage m1;
        
        if(! isEnglish){
            m1.setAddress("/nuup/show/jumpto");
            m1.addStringArg("Act_1/Scene_1");
            boxPlaced = true;
            
        }
        
        if(isEnglish == true){
            m1.setAddress("/nuup/show/jumpto");
            m1.addStringArg("Act_1/Scene_1a");
            boxPlaced = true;
            
        }
        sender.sendMessage(m1);
        
        /*7C00565A81F1
         7C0056935DE4
         7C00564692FE
         7C0056720B53
         7C0056644709
         7C0056596615*/
    }
    
    if(message.find("80000AAE381C" )==true || message.find("7C005754245B")==true || message.find("7E00203583E8")==true || message.find("7E001FD646F1")==true || message.find("7E002007BAE3")==true ||message.find("7E001FE339BB")==true){
        ofBackground(ofRandom(255),ofRandom(255),ofRandom(255));
        ofxOscMessage m1;
        
        if(! isEnglish){
            m1.setAddress("/nuup/show/jumpto");
            m1.addStringArg("Act_1/Scene_2");
            boxPlaced = true;
            
        }
        
        if(isEnglish == true){
            m1.setAddress("/nuup/show/jumpto");
            m1.addStringArg("Act_1/Scene_2a");
            boxPlaced = true;
            
        }
        sender.sendMessage(m1);
        
        /*80000AAE381C
         7C005754245B
         7E00203583E8
         7E001FD646F1
         7E002007BAE3
         7E001FE339BB*/
    }
    
    
    if(message.find("7C00569717AA" )==true || message.find("7C0056769CC0")==true || message.find("7C0056912D96")==true || message.find("7C005653621B")==true || message.find("7C0056949F21")==true ||message.find("7C005768D192")==true){
        ofBackground(ofRandom(255),ofRandom(255),ofRandom(255));
        ofxOscMessage m1;
        
        if(! isEnglish){
            m1.setAddress("/nuup/show/jumpto");
            m1.addStringArg("Act_1/Scene_3");
            boxPlaced = true;
            
        }
        
        if(isEnglish == true){
            m1.setAddress("/nuup/show/jumpto");
            m1.addStringArg("Act_1/Scene_198367");
            boxPlaced = true;
            
        }
        sender.sendMessage(m1);
        
        /* 7C00569717AA
         7C0056769CC0
         7C0056912D96
         7C005653621B
         7C0056949F21
         7C005768D192*/
        
    }
    
    if(message.find("80000AE892F0" )==true || message.find("80000A8E2D29")==true || message.find("82003CDE9DFD")==true || message.find("82003CC795EC")==true || message.find("82003CDADABE")==true ||message.find("82003CABD9CC")==true){
        ofBackground(ofRandom(255),ofRandom(255),ofRandom(255));
        ofxOscMessage m1;
        
        if(! isEnglish){
            m1.setAddress("/nuup/show/jumpto");
            m1.addStringArg("Act_1/Scene_4");
            boxPlaced = true;
            
        }
        
        
        if(isEnglish == true){
            m1.setAddress("/nuup/show/jumpto");
            m1.addStringArg("Act_1/Scene_AC8BB7");
            boxPlaced = true;
            
        }
        sender.sendMessage(m1);
        
        /*80000AE892F0
         80000A8E2D29
         82003CDE9DFD
         82003CC795EC
         82003CDADABE
         82003CABD9CC
         */
    }
    
    serial.flush();
    //serial.drain();
    
    
}


//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
    
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){
    
}


