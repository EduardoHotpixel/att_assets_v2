#pragma once

#include "ofMain.h"
#include "ofxSimpleSerial.h"
#include "ofxOsc.h"
#include "ofxSyphon.h"
#include "ofxHapPlayer.h"
#include "ofxCenteredTrueTypeFont.h"
#include "ofxNetwork.h"
#include "ofxGui.h"
#include "ofxJSON.h"


#define HOST "localhost"
#define PORT 7890
#define RECEIVER_PORT 8888
#define QUERY_INTERVAL 30000

class ofApp : public ofBaseApp{
    
public:
    void setup();
    void update();
    void draw();
    
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    void onNewMessage(string & message);
    void TCPServer();
    void TCPServerLang();
    
    //SHOW DEBUG
    bool showInfo;
    
    //RFID
    ofxOscSender sender;
    ofxOscReceiver receiver;
    ofxSimpleSerial	serial;
    bool requestRead;
    
    ofBuffer serialBuffer;
    ofBuffer arduinoBuffer;
    
   // string message;
    string motorState;
    string screen1State;
    string screen2State;
    string screen3State;
    string screen4State;
    
    bool isActive1;
    bool isActive2;
    bool isActive3;
    bool isActive4;
    bool isEnglish;
    
    //ARDUINO
    
    ofArduino	ard;
    bool		bSetupArduino;			// flag variable for setting up arduino once
    
    //SYPHON
    ofxSyphonServer mainOutputSyphonServer;
    
    //IS TOKEN PLACED
    bool isPlaced;
    
    //TOKEN ANIMATION
    //TOKEN FONT
    ofxCenteredTrueTypeFont tokenFont;
    //PARTICLES VIDEO
    ofxHapPlayer tokenParticles;
    //TOKEN ALPHA
    int tokenAlpha;
    //ARROW TOKEN
    ofxHapPlayer arrow;
    
    //WELCOME ANIMATION
    //WELCOME STATE
    int welcomeState;
    //WELCOME LOGO
    ofxHapPlayer welcomeLogo;
    //WELCOME FONT
    ofxCenteredTrueTypeFont welcomeFont;
    int welcomeAlpha;
    float logoScale;
    
    //DATA
    ofDirectory dataDir;
    vector<ofImage> dataSprite;
    
    float currentData;
    float targetData;
    float remainingDataAlpha;
    float mappedTargetData;
    int mappedCurrentData;
    float startDataTimer;
    bool timerDataEnd;
    float endTimerData;
    int dataAlpha;
    
    ofxCenteredTrueTypeFont remainingDataFont;
    ofxCenteredTrueTypeFont mbDataFont;
    ofxCenteredTrueTypeFont firstNameFont;
    ofxCenteredTrueTypeFont dataMsgFont;
    ofxCenteredTrueTypeFont transactionFont;
    ofxCenteredTrueTypeFont outroMsg1;
    ofxCenteredTrueTypeFont outroMsg2;
    
    //TCP LANG SERVER
    ofxTCPServer TCPLang;
    vector <string> storeTextLang;
    uint64_t lastSentLang;
    string  jsonLang;
    string lang;
    
    //TCP SERVER
    ofxTCPServer TCP;
    vector <string> storeText;
    uint64_t lastSent;
    string  json;
    
    
    string firstName;
    string lastName;
    std::string genre;
    string employer;
    int initialData;
    string transactionData;
    string remainingData;
    string readerIndex;
    int readerStatus;
    bool isUserActive;
    //OUTRO
    float outroAlpha;
    
    ofxPanel gui;
    ofxFloatSlider tokenInstructionX;
    ofxFloatSlider tokenInstructionY;
    ofxFloatSlider instrucccionTokenX;
    ofxFloatSlider instrucccionTokenY;
    ofxFloatSlider welcomeX;
    ofxFloatSlider welcomeY;
    ofxFloatSlider bienvenidoX;
    ofxFloatSlider bienvenidoY;
    ofxFloatSlider welcomeNameX;
    ofxFloatSlider welcomeNameY;
    ofxFloatSlider bienvenidoNombreX;
    ofxFloatSlider bienvenidoNombreY;
    ofxFloatSlider dataX;
    ofxFloatSlider dataY;
    ofxFloatSlider datosX;
    ofxFloatSlider datosY;
    ofxFloatSlider usedDataX;
    ofxFloatSlider usedDataY;
    ofxFloatSlider restanteX;
    ofxFloatSlider restanteY;
    ofxFloatSlider mbX;
    ofxFloatSlider mbY;
    ofxFloatSlider timerScene;
    ofxFloatSlider outroMsgX;
    ofxFloatSlider outroMsgY;
    ofxFloatSlider mensajeDeSalidaX;
    ofxFloatSlider mensajeDeSalidaY;
    ofxFloatSlider mensaje2DeSalidaX;
    ofxFloatSlider mensaje2DeSalidaY;
    ofxFloatSlider outro2MsgX;
    ofxFloatSlider outro2MsgY;
    ofxFloatSlider logoX;
    ofxFloatSlider logoY;
    
    ofxFloatSlider idleInstructionX;
    ofxFloatSlider idleInstructionY;
    ofxFloatSlider idleInstruction2X;
    ofxFloatSlider idleInstruction2Y;
    
    ofxFloatSlider idleInstruccionX;
    ofxFloatSlider idleInstruccionY;
    ofxFloatSlider idleInstruccion2X;
    ofxFloatSlider idleInstruccion2Y;
    ofxFloatSlider dataSpriteOffsetX;
    ofxFloatSlider dataSpriteOffsetY;
    
    float dataTimer;
    bool pause;
    
    ofxHapPlayer idleVideo;
    
    bool boxPlaced;
    float msgAlpha;
   
    bool jsonOK;
    std::string url;
    ofxJSON httpJSON;
    float queryStartTime;
    bool  queryTimerEnd;
    float queryEndTime;
    
private:
    
    void setupArduino(const int & version);
    void digitalPinChanged(const int & pinNum);
    void analogPinChanged(const int & pinNum);
    void updateArduino();
    
    
    
    
};
